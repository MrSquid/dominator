/* Dominator : Falling dominos numerical simulation
*
* Copyright (C) 2018
*
*            Dulac William    Gillet Amaury
*            Sarica Thibaud   Kraiem Samy
*
* dominator is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "domino.h"

using namespace std;

int main(){

    domino A;
    cout << "Collision angle : " << setprecision(10) << A.collision_angle << " rad " << endl << endl;

    while (A.index < A.N_domino){
            cout << "Domino N°" << A.index << endl;
            cout << "Collided at   : " << setprecision(10) << A.elapsed_time << " s " << endl;
            cout << "Fall duration : " << setprecision(10) << A.collision_time << " s  " << endl << endl;
            A.solveTiltOverTime();
            A.iterate();
        }
    A.solveWaveSpeed();
    system("bash plot_first_domino.sh");
    system("bash plot_wave_speed.sh");
    return 0;
}
