#!/usr/bin/bash

gnuplot<<EOF
set xlabel "Time [s]"
set ylabel "Tilt [rad]"
set title "Evolution of the first domino's tilt over time" font "Times, 28"
set terminal postscript eps enhanced color font "Times, 24" linewidth 2
set output "figures/first_domino_tilt.eps"
set size 1.5, 1.5
set autoscale yfixmax
set grid
unset key
plot "output/tilt_domino_0.dat" u 1:2 with lines lc rgb "red" notitle
EOF
