#!/usr/bin/bash

gnuplot<<EOF
set xlabel "Domino from the chain"
set ylabel "Propagation speed [m/s]"
set title "Evolution of the propagation speed within the domino chain" font "Times, 28"
set terminal postscript eps enhanced color font "Times, 24" linewidth 2
set output "figures/wave_speed.eps"
set size 1.5, 1.5
set autoscale xfixmax
set grid
unset key
plot "output/wave_speed.dat" u 1:2 with points lc rgb "red" notitle
EOF
