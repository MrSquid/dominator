/* Dominator : Falling dominos numerical simulation
*
* Copyright (C) 2018
*
*            Dulac William    Gillet Amaury
*            Sarica Thibaud   Kraiem Samy
*
* dominator is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "domino.h"
#include "functions.h"

int func (double t, const double y[], double f[], void* params){

    struct func_params * p =  (struct func_params *) params;
    double J        =   p->J;
    double gamma    =   p->gamma;
    double m        =   p->m;
    double g        =   p->g;
    double h        =   p->h;

    f[0] = y[1];
    f[1] = -(gamma / J)*y[1] + ((m*g*h)/(2*J)) * sin(y[0]); // y[0] holds the angle while y[1] holds the angular speed.

    return GSL_SUCCESS;
}

double collision_function(double x, void* params){

    double h, l0, Delta;
    h       =   CST_HEIGHT;
    l0      =   CST_RESTING_LENGTH;
    Delta   =   CST_DIST;

    return h*sin(x) + l0*cos(x) - Delta;
}
